package states;

import VisitorComposite.*;
import srcclasses.*;
public class CalculateState extends CalcState {

	CalculateVisitor visit=new CalculateVisitor();
	@Override
	public
	void handleOp(Controller a,Operator aOp) {
		
		{a.setState(new StartState());
		a.guiscreen.jTextField1.setText("");
		a.resetModel();
		}
	}

	@Override
	public
	void handleDigit(Controller c,Digits aD) {
		
		FirstOpstate state=new FirstOpstate();
		
		c.guiscreen.jTextField1.setText(""+aD.getValue());
		c.setState(state);
	}


}
