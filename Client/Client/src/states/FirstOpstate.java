package states;

import VisitorComposite.*;
import srcclasses.*;
public class FirstOpstate extends CalcState{
	
	@Override
	public
	void handleOp(Controller a,Operator aOp) {
		if(aOp.getValue()!="C"&&aOp.getValue()!="="){
			String value=a.guiscreen.jTextField1.getText();
			Node n=new Digits(value);
			a.addNode(n);
			a.addNode(aOp);
		WaitSecondOP state=new WaitSecondOP();
		a.setState(state);
		}
		else if(aOp.getValue().equals("="))
		{
			ErrorState state=new ErrorState();
			state.HandleError(a);
		}
		else
			{a.setState(new StartState());
			a.guiscreen.jTextField1.setText("");
			a.resetModel();
			}
	}

	@Override
	public
	void handleDigit(Controller c,Digits aD) {
		c.guiscreen.jTextField1.setText(c.guiscreen.jTextField1.getText()+aD.getValue());
		c.setState(this);
		
	}



}
