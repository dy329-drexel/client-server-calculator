package states;

import VisitorComposite.*;
import srcclasses.*;
public class GetSecondOp extends CalcState{

	@Override
	public
	void handleOp(Controller a,Operator aOp) {
		
		if(aOp.getValue()!="C"&&aOp.getValue()!="="){
			String value=a.guiscreen.jTextField1.getText();
			Node n=new Digits(value);
			a.addNode(n);
			
			int valu=((Controller) a).getCalculation();
			String val=valu+"";
			a.addNode(aOp);
			a.guiscreen.jTextField1.setText(val);
			WaitSecondOP state=new WaitSecondOP();
			a.setState(state);
		}
		else if(aOp.getValue().equals("="))
		{
			String value=a.guiscreen.jTextField1.getText();
			Node n=new Digits(value);
			a.addNode(n);
			
		
			int valu=((Controller) a).getCalculation();
			a.addNode(aOp);
			System.out.println("value is"+valu);
			String val=valu+"";
			a.addNode(new Digits(val));
			a.printVisitor();
			a.guiscreen.jTextField1.setText(val);
			CalculateState state=new CalculateState();
			a.setState(state);
			
		}
		else
		{a.setState(new StartState());
		a.guiscreen.jTextField1.setText("0");
		a.resetModel();
		}
		
		
		}

	@Override
	public
	void handleDigit(Controller c,Digits aD) {
		c.guiscreen.jTextField1.setText(c.guiscreen.jTextField1.getText()+aD.getValue());
		c.setState(this);
		
	}



}
