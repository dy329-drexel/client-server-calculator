package states;


import VisitorComposite.*;
import srcclasses.*;
public class StartState extends CalcState{

	

	@Override
	public
	void handleOp(Controller a,Operator aOp) {
		if(aOp.getValue().equals("="))
			{ErrorState state=new ErrorState();
			state.HandleError(a);
			}
	}

	@Override
	public
	void handleDigit(Controller c,Digits aD) {
		FirstOpstate state=new FirstOpstate();
		
		c.guiscreen.jTextField1.setText(""+aD.getValue());
		c.setState(state);
		
		
	}

}
