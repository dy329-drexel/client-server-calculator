package states;

import javax.swing.JOptionPane;

import VisitorComposite.*;
import srcclasses.*;
public class WaitSecondOP extends CalcState{

	@Override
	public
	void handleOp(Controller a,Operator aOp) {
		if(aOp.getValue().equals("C"))
		{
			a.setState(new StartState());
			a.guiscreen.jTextField1.setText("");
			a.resetModel();
		}else
		{
		ErrorState state=new ErrorState();
		state.HandleError(a);
		
		
		}
				
	}

	@Override
	public
	void handleDigit(Controller c,Digits aD) {
		GetSecondOp state=new GetSecondOp();
		c.setState(state);
		c.guiscreen.jTextField1.setText(""+aD.getValue());
		
	}



}
