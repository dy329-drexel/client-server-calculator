package states;


import VisitorComposite.*;
import srcclasses.*;

public abstract class CalcState {

	public abstract void handleOp(Controller  a,Operator aOp);
	public abstract void handleDigit(Controller  c,Digits aD);
	
	
}
