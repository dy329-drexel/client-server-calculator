package VisitorComposite;

import java.util.ArrayList;
import java.util.List;

public class CalculateVisitor implements Visitor{
	List<Integer> operands = new ArrayList<>();
	List<String> operators = new ArrayList<>();
	
	public CalculateVisitor()
	{
		 operands = new ArrayList<>();
		operators = new ArrayList<>();
	}
	@Override
	public void visitOperator(Operator f) {
	
		String s = f.getValue();
		//skip root node
		if(s!=">")
		operators.add(s);
	
}
	

	@Override
	public void visitDigit(Digits fol) {
		int val = Integer.parseInt(fol.getValue());
		operands.add(val);
		
	}


	public int getResult() {
		
		//calculating the result using the node structure
		while ((operands.size() > 1)&&(operators.size()>0)){
			
			int val1 = operands.get(0);
			operands.remove(0);
			int val2 = operands.get(0);
			operands.remove(0);
			String op = operators.get(0);
			operators.remove(0);
			int total = calculate(val1, val2, op);
		
			operands.add(0, total);

		}
		return operands.get(0);
		
	}
private int calculate(int val1, int val2, String operators) {
		
		
		if(operators.equals("+"))
			return val1+val2;
		if(operators.equals("-"))
			return val1-val2;
		if(operators.equals("*"))
			return val1*val2;
		if(operators.equals("/"))
			return val1/val2;
		
		else return 0;

	}

}
