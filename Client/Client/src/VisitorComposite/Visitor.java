package VisitorComposite;

public interface Visitor {
	public void visitOperator(Operator f);
	public void visitDigit(Digits fol);
}
