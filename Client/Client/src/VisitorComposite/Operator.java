package VisitorComposite;
import java.util.ArrayList;
import java.util.Date;

public class Operator extends Node{

	
	public String name;

	public ArrayList<Node> list=new ArrayList<Node>();	
	public Operator(String a)	{
		name=a;
	}

	
		@Override
		public void acceptVisitor(Visitor v) {
		
			v.visitOperator(this);
			//visit every component
		
			for(int i=0;i<list.size();i++){
				
					
				list.get(i).acceptVisitor(v);
			}
			
			
		}

		@Override
		public void add(Node c) {
	
		list.add(c);
			
		}

		@Override
		public void remove() {
			
		list.remove((list.size()-1));
			
		

		
}


		@Override
		public String getValue() {
			// TODO Auto-generated method stub
			return name;
		}
}
