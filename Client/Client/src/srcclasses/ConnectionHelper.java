package srcclasses;

import java.io.IOException;

import java.io.ObjectOutputStream;
import java.net.Socket;



public class ConnectionHelper {
	static Socket socket_;
	ObjectOutputStream outps ;
	
	private void connect(){
		 try {
			socket_ = new Socket("127.0.0.1", 1900);
			
			outps= new ObjectOutputStream(socket_.getOutputStream());
			
		
		} catch (IOException e ) {
			System.out.println("Connection error");
		
			
		}
		
		
		
	}
	
	public void writeCalculation(String calc){
		connect();
		try {
		
			outps.writeObject(calc);
			closeConnection();
			
		} catch (IOException e) {
			
		}
		catch(Exception e){
			
			
		}
		
	}


	
	public void closeConnection(){
		try {
			
			
			
			socket_.close();
			outps.close();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		
		
	}
	
}
