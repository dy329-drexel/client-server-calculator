package srcclasses;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.LayoutManager;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;



import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JScrollPane;

public class CalculaterGui extends JFrame{
	/**
	 * 
	 */
	
	  // Variables declaration - do not modify                     
    javax.swing.JButton add;
     javax.swing.JButton cancel;
     javax.swing.JButton divide;
     javax.swing.JButton eight;
     javax.swing.JButton equals;
    javax.swing.JButton five;
     javax.swing.JButton four;
     javax.swing.JButton nine;
     javax.swing.JPanel jPanel1;
   public  javax.swing.JTextField jTextField1;
     javax.swing.JButton minus;
     javax.swing.JButton multiply;
     javax.swing.JButton one;
    javax.swing.JButton seven;
     javax.swing.JButton six;
    javax.swing.JButton three;
    javax.swing.JButton two;
    javax.swing.JButton zero;
	private static final long serialVersionUID = 1L;

	public CalculaterGui() {
		
		//Setting up GUI
		  jPanel1 = new javax.swing.JPanel();
	        one = new javax.swing.JButton();
	        two = new javax.swing.JButton();
	        three = new javax.swing.JButton();
	        four = new javax.swing.JButton();
	        five = new javax.swing.JButton();
	        six = new javax.swing.JButton();
	        seven = new javax.swing.JButton();
	        eight = new javax.swing.JButton();
	        nine = new javax.swing.JButton();
	        zero = new javax.swing.JButton();
	        equals = new javax.swing.JButton();
	        cancel = new javax.swing.JButton();
	        add = new javax.swing.JButton();
	        minus = new javax.swing.JButton();
	        multiply = new javax.swing.JButton();
	        divide = new javax.swing.JButton();
	        jTextField1 = new javax.swing.JTextField();

	        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
	        setBackground(new java.awt.Color(153, 153, 255));

	        jPanel1.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(153, 0, 0)));

	        one.setBackground(new java.awt.Color(0, 51, 204));
	        one.setFont(new java.awt.Font("Monotype Corsiva", 1, 18)); // NOI18N
	        one.setForeground(new java.awt.Color(255, 255, 255));
	        one.setText("1");
	      

	        two.setBackground(new java.awt.Color(0, 51, 204));
	        two.setFont(new java.awt.Font("Monotype Corsiva", 1, 18)); // NOI18N
	        two.setForeground(new java.awt.Color(255, 255, 255));
	        two.setText("2");
	    

	        three.setBackground(new java.awt.Color(0, 51, 204));
	        three.setFont(new java.awt.Font("Monotype Corsiva", 1, 18)); // NOI18N
	        three.setForeground(new java.awt.Color(255, 255, 255));
	        three.setText("3");
	      

	        four.setBackground(new java.awt.Color(0, 51, 204));
	        four.setFont(new java.awt.Font("Monotype Corsiva", 1, 18)); // NOI18N
	        four.setForeground(new java.awt.Color(255, 255, 255));
	        four.setText("4");
	     
	        five.setBackground(new java.awt.Color(0, 51, 204));
	        five.setFont(new java.awt.Font("Monotype Corsiva", 1, 18)); // NOI18N
	        five.setForeground(new java.awt.Color(255, 255, 255));
	        five.setText("5");
	       

	        six.setBackground(new java.awt.Color(0, 51, 204));
	        six.setFont(new java.awt.Font("Monotype Corsiva", 1, 18)); // NOI18N
	        six.setForeground(new java.awt.Color(255, 255, 255));
	        six.setText("6");
	        

	        seven.setBackground(new java.awt.Color(0, 51, 204));
	        seven.setFont(new java.awt.Font("Monotype Corsiva", 1, 18)); // NOI18N
	        seven.setForeground(new java.awt.Color(255, 255, 255));
	        seven.setText("7");
	       

	        eight.setBackground(new java.awt.Color(0, 51, 204));
	        eight.setFont(new java.awt.Font("Monotype Corsiva", 1, 18)); // NOI18N
	        eight.setForeground(new java.awt.Color(255, 255, 255));
	        eight.setText("8");
	        

	        nine.setBackground(new java.awt.Color(0, 51, 204));
	        nine.setFont(new java.awt.Font("Monotype Corsiva", 1, 18)); // NOI18N
	        nine.setForeground(new java.awt.Color(255, 255, 255));
	        nine.setText("9");
	        
	        zero.setBackground(new java.awt.Color(0, 51, 204));
	        zero.setFont(new java.awt.Font("Monotype Corsiva", 1, 18)); // NOI18N
	        zero.setForeground(new java.awt.Color(255, 255, 255));
	        zero.setText("0");
	      

	        equals.setBackground(new java.awt.Color(0, 51, 204));
	        equals.setFont(new java.awt.Font("Monotype Corsiva", 1, 18)); // NOI18N
	        equals.setForeground(new java.awt.Color(255, 255, 255));
	        equals.setText("=");
	        

	        cancel.setBackground(new java.awt.Color(0, 51, 204));
	        cancel.setFont(new java.awt.Font("Monotype Corsiva", 1, 18)); // NOI18N
	        cancel.setForeground(new java.awt.Color(255, 255, 255));
	        cancel.setText("C");
	       

	        add.setBackground(new java.awt.Color(0, 51, 204));
	        add.setFont(new java.awt.Font("Monotype Corsiva", 1, 18)); // NOI18N
	        add.setForeground(new java.awt.Color(255, 255, 255));
	        add.setText("+");
	        

	        minus.setBackground(new java.awt.Color(0, 51, 204));
	        minus.setFont(new java.awt.Font("Monotype Corsiva", 1, 18)); // NOI18N
	        minus.setForeground(new java.awt.Color(255, 255, 255));
	        minus.setText("-");
	       

	        multiply.setBackground(new java.awt.Color(0, 51, 204));
	        multiply.setFont(new java.awt.Font("Monotype Corsiva", 1, 18)); // NOI18N
	        multiply.setForeground(new java.awt.Color(255, 255, 255));
	        multiply.setText("*");
	       

	        divide.setBackground(new java.awt.Color(0, 51, 204));
	        divide.setFont(new java.awt.Font("Monotype Corsiva", 1, 18)); // NOI18N
	        divide.setForeground(new java.awt.Color(255, 255, 255));
	        divide.setText("/");
	      

	        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
	        jPanel1.setLayout(jPanel1Layout);
	        jPanel1Layout.setHorizontalGroup(
	            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(jPanel1Layout.createSequentialGroup()
	                .addGap(20, 20, 20)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
	                    .addGroup(jPanel1Layout.createSequentialGroup()
	                        .addComponent(zero)
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                        .addComponent(equals)
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                        .addComponent(cancel))
	                    .addGroup(jPanel1Layout.createSequentialGroup()
	                        .addComponent(seven)
	                        .addGap(18, 18, 18)
	                        .addComponent(eight)
	                        .addGap(18, 18, 18)
	                        .addComponent(nine))
	                    .addGroup(jPanel1Layout.createSequentialGroup()
	                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
	                            .addComponent(four)
	                            .addComponent(one))
	                        .addGap(18, 18, 18)
	                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                            .addGroup(jPanel1Layout.createSequentialGroup()
	                                .addComponent(two)
	                                .addGap(18, 18, 18)
	                                .addComponent(three))
	                            .addGroup(jPanel1Layout.createSequentialGroup()
	                                .addComponent(five)
	                                .addGap(18, 18, 18)
	                                .addComponent(six)))))
	                .addGap(38, 38, 38)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addGroup(jPanel1Layout.createSequentialGroup()
	                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
	                            .addComponent(multiply, javax.swing.GroupLayout.DEFAULT_SIZE, 49, Short.MAX_VALUE)
	                            .addComponent(divide, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	                        .addGap(0, 0, Short.MAX_VALUE))
	                    .addGroup(jPanel1Layout.createSequentialGroup()
	                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                            .addComponent(add, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
	                            .addComponent(minus, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
	                        .addContainerGap(40, Short.MAX_VALUE))))
	        );
	        jPanel1Layout.setVerticalGroup(
	            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(jPanel1Layout.createSequentialGroup()
	                .addGap(31, 31, 31)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(one)
	                    .addComponent(two)
	                    .addComponent(three)
	                    .addComponent(add))
	                .addGap(27, 27, 27)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(four)
	                    .addComponent(five)
	                    .addComponent(six)
	                    .addComponent(minus))
	                .addGap(28, 28, 28)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(seven)
	                    .addComponent(eight)
	                    .addComponent(nine)
	                    .addComponent(multiply))
	                .addGap(27, 27, 27)
	                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(zero)
	                    .addComponent(equals)
	                    .addComponent(cancel)
	                    .addComponent(divide))
	                .addContainerGap(29, Short.MAX_VALUE))
	        );
	       
	        jTextField1.setEditable(false);
	        jTextField1.setBackground(new java.awt.Color(255, 255, 255));
	        jTextField1.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
	        jTextField1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

	        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
	        getContentPane().setLayout(layout);
	        layout.setHorizontalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addComponent(jTextField1)
	            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	        );
	        layout.setVerticalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
	                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
	                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	        );

	        pack();
		

	   this.setVisible(true);
	   Controller cn=new Controller();
	   cn.register(this);
	  

	}
public int showError(){
	//display error message dialog box
		Object[] options = {"Reset","Discard"};
            
		int n = JOptionPane.showOptionDialog(null,"error","Error", JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE,
				null,
				options,
				options[1]);
				return n;
		
	}
	
}
