package srcclasses;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import VisitorComposite.*;
import states.CalcState;
import states.*;


public class Controller {
	public CalculaterGui guiscreen=null;
	 CalcState state=new StartState();
	 ExpressionModel em=new ExpressionModel();
	 ConnectionHelper ch=new ConnectionHelper();
	 
	 public void addNode(Node a){
	 em.addNode(a);
	 }
	 
	 
	 public void resetModel(){
	  em.resetModel();
	 }

	  public void printVisitor(){
	
	 PrintVisitor pv=new PrintVisitor();
	 em.getModel().acceptVisitor(pv);
	 System.out.println(pv.a);
	 String temp=pv.a.replaceAll("null>","");
	 System.out.println(temp);
	 ch.writeCalculation(temp);
	 resetModel();
	 }
	
	
	public void setState(CalcState aState){
	state=aState;
		}
	
	public void register(CalculaterGui gui){
		guiscreen=gui;
		
		 ButtonHandler buttonHandler = new ButtonHandler();
			OperationHandler operationHandler=new OperationHandler();
			
			//adding listners to all buttons of gui	
		 guiscreen.cancel.addActionListener(operationHandler);
		 guiscreen.equals.addActionListener(operationHandler);		
		 guiscreen.add.addActionListener(operationHandler);
		 guiscreen.minus.addActionListener(operationHandler);
		 guiscreen.multiply.addActionListener(operationHandler);
		 guiscreen.divide.addActionListener(operationHandler);
		 
		 guiscreen.one.addActionListener(buttonHandler);
		 guiscreen.two.addActionListener(buttonHandler);
		 guiscreen.three.addActionListener(buttonHandler);
		 guiscreen.four.addActionListener(buttonHandler);
		 guiscreen.five.addActionListener(buttonHandler);
		 guiscreen.six.addActionListener(buttonHandler);
		 guiscreen.seven.addActionListener(buttonHandler);
		 guiscreen.eight.addActionListener(buttonHandler);
		 guiscreen.nine.addActionListener(buttonHandler);
		 guiscreen.zero.addActionListener(buttonHandler);
}
	//inner classes for listners
		class ButtonHandler implements ActionListener
		{
			public void actionPerformed(ActionEvent e)
			{
				
					Object source = e.getSource();
					Digits no=null;
		  
					if(source==guiscreen.one)
					{
							no=new Digits("1");
						state.handleDigit(Controller.this,no);
						
						
					}
					else if(source==guiscreen.two)
						
					{
						no=new Digits("2");
						state.handleDigit(Controller.this,no);
						
						
					}
					else  if(source==guiscreen.three)
					{
						no=new Digits("3");
						state.handleDigit(Controller.this,no);
						
						
					}
					else  if(source==guiscreen.four)
					{
						no=new Digits("4");
					state.handleDigit(Controller.this,no);
						
						
					}
					else 	if(source==guiscreen.five)
					{
						no=new Digits("5");
					state.handleDigit(Controller.this,no);
					
						
						
					}
					else  if(source==guiscreen.six)
					{
						no=new Digits("6");
						state.handleDigit(Controller.this,no);
					
						
					}
					else if(source==guiscreen.seven)
					{
						no=new Digits("7");
					state.handleDigit(Controller.this,no);
						
						
						
					}
					else  if(source==guiscreen.eight)
					{
						no=new Digits("8");
					state.handleDigit(Controller.this,no);
					
						
					}
					else if(source==guiscreen.nine)
						
					{
						no=new Digits("9");
						state.handleDigit(Controller.this,no);
						
						
					}
					else if(source==guiscreen.zero)
						
					{
						no=new Digits("0");
						state.handleDigit(Controller.this,no);
						
					
					}
					
				
			}
}
		class OperationHandler implements ActionListener
		{
			public void actionPerformed(ActionEvent e)
			{
				Operator no=null;
					Object source = e.getSource();
					
		  
					if(source==guiscreen.add)
					{
					no=new Operator("+");
					state.handleOp(Controller.this,no);
					}	
					
					else if(source==guiscreen.minus)
						{
							no=new Operator("-");
							state.handleOp(Controller.this,no);
						
						}
					
					else  if(source==guiscreen.multiply)
						{
							no=new Operator("*");
							state.handleOp(Controller.this,no);
						
						}
					
					else  if(source==guiscreen.divide)
					{
					
						no=new Operator("/");
						state.handleOp(Controller.this,no);
					}
					
					
					else 	if(source==guiscreen.cancel)
						{
						
							no=new Operator("C");
							state.handleOp(Controller.this,no);
						}
					
					else  if(source==guiscreen.equals)
						{
							no=new Operator("=");
							state.handleOp(Controller.this,no);
						}
				
					
				
					
				
			}
}
	
//method to calculate
		public int getCalculation() {
				CalculateVisitor v=new CalculateVisitor();
				em.getModel().acceptVisitor(v);
	 
			 	return v.getResult();
		}




		}
		
